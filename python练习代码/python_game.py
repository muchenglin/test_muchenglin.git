#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/6/2 21:15
# @Author  : muchenmglin
class Hero:
    hero_hp=0
    hero_power=0
    hero_name=''
    def fight(self,enemy_hp,enemy_power,enemy_name):
        '''

        :param enemy_hp:敌人血量
        :param enemy_power: 敌人攻击力
        :return:
        '''
        # 制定游戏回合最大局数为30
        for i in range(101):
            hero_final_hp = self.hero_hp - enemy_power
            enemy_final_hp = enemy_hp - self.hero_power
            print('第',i,'回合')
            print(f'当前{self.hero_name}的血量：{hero_final_hp}')
            print(f'当前{enemy_name}的血量：{enemy_final_hp}')
            # 控制回合局数，当前局数超过30局为平局
            if i>30:
                print('回合结束,双方战平')
                break
            # 判断当前英雄血量小于0以下，返回敌人赢了,跳出循环
            if hero_final_hp<=0 :
                print(f'{enemy_name}赢了')
                break
            # 判断当前英雄血量小于0以下，返回英雄赢了，跳出循环
            elif enemy_final_hp<=0:
                print(f'{self.hero_name}赢了')
                break
            # 每回合赋予hero_hp的值为扣除后的血量
            self.hero_hp = hero_final_hp
            # 每回合赋予enemy_hp的值为扣除后的血量
            enemy_hp = enemy_final_hp


class angle(Hero):
    hero_hp=1000
    hero_power = 20
    hero_name='天使'
class devil(Hero):
    hero_hp=1000
    hero_power =25
    hero_name='恶魔'




