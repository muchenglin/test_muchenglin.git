#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/6/2 21:49
# @Author  : muchenmglin
from python_game import angle,devil
class hero_factory:
    def create_hero(self,hero):
        if hero=='angle':
            #返回实例对象
            return angle()
        elif hero=='devil':
            # 返回实例对象
            return devil()
        else:
            raise Exception('此英雄不在英雄池中')
hero_factory=hero_factory()
angle=hero_factory.create_hero('angle')
devil = hero_factory.create_hero('devil')
angle.fight(devil.hero_hp,devil.hero_power,devil.hero_name)
