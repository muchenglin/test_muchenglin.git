#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/7/11 10:12 下午
# @Author  : muchenglin
# @File    : test_weixin.py
# @Software: PyCharm
import pytest

from Weixin_web.po.contact_page import ContactPage
from Weixin_web.po.main_page import MainPage

'''
participant 企业微信主页面 as main
 
participant 通讯录页面 as contact
 
participant 添加成员页面 as add_member
 
main -> contact: 点击通讯录
 
main -> add_member: 点击添加成员
 
contact -> add_member: 点击添加成员
 
add_member -> contact: 填写成员资料， 点击保存
'''


# 1、跳转到添加成员界面
# 2、添加新成员，点击保存返回通讯录页面
# 在通讯录页面获取成员新。添加断言判断添加成员是否成功
# 通过链式调用详细描述业务逻辑
class TestWeixin():
    # 添加一个成员测试用例
    @pytest.mark.parametrize("name",["十五"])
    def test_addMember_succes(self,name,acctid,phone):
        main_page = MainPage()
        # 添加断言，判断联系人是否添加成功
        # 业务逻辑：通过主页添加成员，跳转到通讯录页面，最后添加断言判断添加成员是否成功
        assert name in main_page.goto_addmember().add_member(name).get_memberlist()
    @pytest.mark.parametrize("name",["十五2"])
    def test_addMember_fail(self,name):
        main_page = MainPage()
        assert name not in main_page.goto_addmember().add_member_fail(name).get_memberlist()
