#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/7/11 10:08 下午
# @Author  : muchenglin
# @Software: PyCharm
from selenium.webdriver.common.by import By

from Weixin_web.po.BasePage import BasePage
from Weixin_web.po.add_member_page import AddMemberPage

class ContactPage(BasePage):
    # 通讯录地址
    _base_url = "https://work.weixin.qq.com/wework_admin/frame#contacts"
    def goto_addmember(self):
        '''
        跳转添加成员页面
        :return:
        '''
        # 写在方法内目的解决，循环导入报错问题
        return AddMemberPage(self.driver)

    def get_memberlist(self):
        '''
        获取成员列表
        :return:
        '''
        ele = self.driver.find_elements(By.CSS_SELECTOR, ".member_colRight_memberTable_td:nth-child(2)")
        # 获取列表成员信息,返回为列表格式
        name_list = [i.text for i in ele]
        print(name_list)
        return name_list