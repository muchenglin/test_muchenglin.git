#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/7/11 9:55 下午
# @Author  : muchenglin
# @Software: PyCharm
from selenium import webdriver

# 企业微信主页面
from selenium.webdriver.common.by import By

from Weixin_web.po.BasePage import BasePage
from Weixin_web.po.add_member_page import AddMemberPage
from Weixin_web.po.contact_page import ContactPage


class MainPage(BasePage):
    # 企业微信首页地址
    base_url = "https://work.weixin.qq.com/wework_admin/frame#index"
    def goto_addmember(self):
        '''
        跳转到添加成员页面
        :return:
        '''
        # 点击添加成员
        self.driver.find_element(By.CLASS_NAME, "ww_indexImg_AddMember").click()
        return AddMemberPage(self.driver)

    def goto_contact(self):
        '''
        跳转到通讯录页面
        :return:
        '''
        return ContactPage(self.driver)