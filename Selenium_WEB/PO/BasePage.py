#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/7/11 9:56 下午
# @Author  : muchenglin
# @File    : BasePage.py
# @Software: PyCharm
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver


class BasePage:
    '''
    basepage 提供公共方法的封装，解决 driver初始化的问题
    '''
    # 添加初始化方法
    _base_url = ""
    def __init__(self,bash_driver = None):
        if bash_driver is None:
            # 通过remote复用浏览器操作
            chrome_arg = webdriver.ChromeOptions()
            # 加入调试地址
            chrome_arg.debugger_address = "127.0.0.1:9222"
            # 实例化driver对象
            self.driver = webdriver.Chrome(options=chrome_arg)
            # 打开首页
            # self.driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
            self.driver.get(self.base_url)
            self.driver.implicitly_wait(5)
        else:
            # 将self.driver 添加一个webdrive对象注解，解决类型提示的问题
            # 注解本身无赋值作用，没有含义
            self.driver: WebDriver = bash_driver
    def find(self,by,location = None):
        '''
        添加判断，适配不同场景场景传入的参数值不同，造成不匹配的问题
        :param by:
        :param location:
        :return:
        '''
        # 只传入一个元组参数
        if location is None:
            return self.driver.find_element(*by)
        else:
            return self.driver.find_element(by=by,value=location)