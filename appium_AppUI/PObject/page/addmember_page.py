#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/9/21 23:12
# @Author  : muchenmglin
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver

from App.PObject.page.base_page import BasePage


class AddMemberPage(BasePage):



    def click_addmember_menual(self):
         # 点击操作[手动输入添加]
        from App.PObject.page.editmember_page import EditMemberPage
        self.find_and_click(MobileBy.XPATH, "//*[@text='手动输入添加']")
        return EditMemberPage(self.driver)

    def get_tip(self):
        result = self.find(MobileBy.XPATH, "//*[@class='android.widget.Toast']").get_attribute("text")
        return result