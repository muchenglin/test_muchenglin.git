#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/9/25 23:54
# @Author  : muchenmglin
from time import sleep

from appium.webdriver.common.mobileby import MobileBy

from App.PObject.page.addresslist_page import AddressListPage
from App.PObject.page.base_page import BasePage



class DelmemberPage(BasePage):

    delmember_element = (MobileBy.XPATH, "//*[@text='确定']")

    def del_member(self):
        self.driver.find_element_by_android_uiautomator(('new UiScrollable(new UiSelector().'
                                                         'scrollable(true).instance(0)).'
                                                         'scrollIntoView(new UiSelector().text("删除成员").'
                                                         'instance(0))')).click()
        sleep(2)
        self.find_and_click(*self.delmember_element)
        return AddressListPage()