#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/9/21 23:11
# @Author  : muchenmglin
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver
from App.PObject.page.addmember_page import AddMemberPage
from App.PObject.page.base_page import BasePage


class AddressListPage(BasePage):
    # addmember_element = (MobileBy.XPATH, "//*[@text='添加成员']")



    def goto_addmember(self):
        # 点击操作[添加成员]
        # self.find_and_click(*self.addmember_element)
        #滑动查找页面元素
        # self.driver.find_element_by_android_uiautomator(('new UiScrollable(new UiSelector().'
        #                                                  'scrollable(true).instance(0)).'
        #                                                  'scrollIntoView(new UiSelector().text("添加成员").'
        #                                                  'instance(0))')).click()
        # self.driver.find_element(MobileBy.XPATH, "//*[@text='添加成员']").click()
        self.find_uiautomator()
        return AddMemberPage(self.driver)

    def isfind_member(self,ele):
        source = self.driver.page_source
        if ele in source:
            return True
        else:
            return False