#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/9/21 23:17
# @Author  : muchenmglin
from appium.webdriver.common.mobileby import MobileBy

from App.PObject.page.addmember_page import AddMemberPage
from appium.webdriver.webdriver import WebDriver

from App.PObject.page.base_page import BasePage


class EditMemberPage(BasePage):

    def edit_member(self,name,phonenum):
        '''
        1，输入姓名
        2，输入手机号
        3，点击保存
        :return:
        '''
        self.find_and_send(MobileBy.XPATH,
                                 "//*[contains(@text,'姓名　')]/../android.widget.EditText",name)
        self.find_and_send(MobileBy.XPATH,
                                 "//*[contains(@text,'手机')]/..//*[@text='必填']",phonenum)
        self.find_and_click(MobileBy.XPATH,"//*[@text='保存']")
        return AddMemberPage(self.driver)

