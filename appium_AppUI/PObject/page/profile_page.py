#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/9/25 23:42
# @Author  : muchenmglin
from time import sleep

from appium.webdriver.common.mobileby import MobileBy

from App.PObject.page.base_page import BasePage
from App.PObject.page.editprofile_page import EditProfilePage


class ProfilePage(BasePage):
    # profile_element = (MobileBy.XPATH, "//*[@text='贺健']")
    profile_user_element = (MobileBy.XPATH, "//*[@resource-id='com.tencent.wework:id/izo']")

    def go_profile_user(self):

        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.tencent.wework:id/izo']").click()
        self.find_and_click(*self.profile_user_element)
        return EditProfilePage()
