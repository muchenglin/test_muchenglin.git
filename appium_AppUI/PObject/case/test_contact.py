#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/9/21 23:19
# @Author  : muchenmglin
import allure
from faker import Faker

from App.PObject.page.app import App
import sys

print(sys.path)

@allure.feature("企业微信UI自动化")
class TestContact:
    def setup_class(self):
        self.fake = Faker("zh_CN")
        self.app = App()
    def setup(self):
        # driver得初始化，App启动
        self.main = self.app.start()

    def teardown(self):
        self.app.back()

    def teardown_class(self):
        # app的关闭，driver销毁
        self.app.stop()
    # test添加联系人
    @allure.title("添加成功")
    @allure.story("添加联系人")
    def test_addcontact(self):
        name = self.fake.name()
        phonenum = self.fake.phone_number()
        with allure.step("获取当前toast信息"):
            result = self.main.goto_main().goto_addresslist().goto_addmember().\
                click_addmember_menual().edit_member(name,phonenum).get_tip()
        with allure.step("添加联系成功"):
            assert result == '添加成功'

    # test删除联系人
    @allure.title("删除成功")
    @allure.story("添加联系人")
    def test_delcontact(self):
        result = self.main.goto_main().goto_profile().go_profile_user().\
            edit_user().del_member().isfind_member()
        assert result == True
